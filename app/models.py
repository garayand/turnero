from django.db import models


class Permiso(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    codigo = models.CharField(max_length=50)
    activo = models.BooleanField()
    # roles = models.ManyToManyField(Rol)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'permiso'
        verbose_name = 'permiso'
        verbose_name_plural = 'permisos'


class Rol(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=100)
    activo = models.BooleanField()
    permisos = models.ManyToManyField(Permiso)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'rol'
        verbose_name = 'rol'
        verbose_name_plural = 'roles'


class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    usuario = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=100)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    roles = models.ManyToManyField(Rol)

    def __str__(self):
        return self.usuario

    class Meta:
        managed = True
        db_table = 'usuario'
        verbose_name = 'usuario'
        verbose_name_plural = 'usuarios'


class Departamento(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=100)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = True
        db_table = 'departamento'
        verbose_name = 'departamento'
        verbose_name_plural = 'departamentos'


class Area(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    codigo_area = models.CharField(max_length=4)
    departamento=models.ForeignKey('Departamento', on_delete=models.CASCADE)
    usuario_modificacion = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=100)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'area'
        verbose_name = 'area'
        verbose_name_plural = 'areas'


class Servicio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    usuario_modificacion = models.CharField(max_length=50)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=50)
    fecha_insercion = models.DateTimeField()

    def __str__(self):
        return self.nombre

    class Meta:
        managed = True
        db_table = 'servicio'
        verbose_name = 'servicio'
        verbose_name_plural = 'servicios'


class ColaAtencion(models.Model):
    id = models.AutoField(primary_key=True)
    nivel_prioridad = models.ForeignKey('NivelPrioridad', on_delete=models.CASCADE, default=2)
    descripcion = models.CharField(max_length=100)
    codigo = models.CharField(max_length=10)
    tickets_creados = models.IntegerField(default=0)
    last_reset = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=100)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    servicios = models.ManyToManyField(Servicio)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'cola_atencion'
        verbose_name = 'cola de atencion'
        verbose_name_plural = 'colas de atencion'




class TipoPersona(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=100)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'tipo_persona'
        verbose_name = 'tipo de persona'
        verbose_name_plural = 'tipos de persona'


class Persona(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_persona=models.ForeignKey('TipoPersona', on_delete=models.CASCADE)
    usuario=models.OneToOneField('Usuario', on_delete=models.CASCADE)
    nombres = models.CharField(max_length=50, blank=True, null=True)
    apellidos = models.CharField(max_length=50, blank=True, null=True)
    documento = models.CharField(max_length=50, blank=True, null=True)
    sexo = models.CharField(max_length=1, blank=True, null=True)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    telefono = models.CharField(max_length=100, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.nombres + " " + self.apellidos

    class Meta:
        managed = True
        db_table = 'persona'
        verbose_name = 'persona'
        verbose_name_plural = 'personas'

class CategoriaCliente(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    codigo_categoria = models.CharField(max_length=3, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'categoria_cliente'
        verbose_name = 'categoria de cliente'
        verbose_name_plural = 'categorias de cliente'


class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    categoria_cliente=models.ForeignKey('CategoriaCliente', on_delete=models.CASCADE)
    persona = models.ForeignKey('Persona', on_delete=models.CASCADE)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.persona.nombres + " " + self.persona.apellidos

    class Meta:
        managed = True
        db_table = 'cliente'
        verbose_name = 'cliente'
        verbose_name_plural = 'clientes'


class Funcionario(models.Model):
    id = models.AutoField(primary_key=True)
    persona=models.OneToOneField('Persona', on_delete=models.CASCADE)
    fecha_ingreso = models.DateField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.persona.nombres + " " + self.persona.apellidos

    class Meta:
        managed = True
        db_table = 'funcionario'
        verbose_name = 'funcionario'
        verbose_name_plural = 'funcionarios'


class TicketAtencion(models.Model):
    id = models.AutoField(primary_key=True)
    estado_ticket=models.ForeignKey('EstadoTicket', on_delete=models.CASCADE)
    servicio = models.ForeignKey('Servicio', on_delete=models.CASCADE)
    funcionario = models.ForeignKey('Funcionario', on_delete=models.CASCADE, blank=True, null=True)
    cliente=models.ForeignKey('Cliente', on_delete=models.CASCADE)
    prefijo = models.TextField(max_length=20)
    numero_orden = models.TextField(max_length=100)
    fechahora_ingreso = models.DateTimeField(blank=True, null=True)
    fechahora_atencion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.prefijo + self.numero_orden

    class Meta:
        managed = True
        db_table = 'ticket_atencion'
        verbose_name = 'ticket de atencion'
        verbose_name_plural = 'tickets de atencion'


class TicketCaller(models.Model):
    id = models.AutoField(primary_key=True)
    ticket = models.ForeignKey('TicketAtencion', on_delete=models.CASCADE)
    last_call = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'ticket_caller'


class Box(models.Model):
    id = models.AutoField(primary_key=True)
    estado_box = models.ForeignKey('EstadoBox', on_delete=models.CASCADE)
    area = models.ForeignKey('Area', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=15, blank=True, null=True)
    codigo = models.CharField(max_length=4, blank=True, null=True)
    numero = models.BigIntegerField(blank=True, null=True)
    color = models.CharField(max_length=10, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=100)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    tickets_llamados = models.ManyToManyField(TicketAtencion)

    def __str__(self):
        return self.codigo + str(self.numero)

    class Meta:
        managed = True
        db_table = 'box'
        verbose_name = 'box'
        verbose_name_plural = 'boxes'


class NivelPrioridad(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'nivel_prioridad'
        verbose_name = 'nivel de prioridad'
        verbose_name_plural = 'niveles de prioridad'


class BoxCola(models.Model):
    id = models.AutoField(primary_key=True)
    box_id = models.ForeignKey('Box', on_delete=models.CASCADE)
    cola_id = models.ForeignKey('ColaAtencion', on_delete=models.CASCADE)
    orden_atencion = models.IntegerField(blank=True, null=True, unique=True)

    class Meta:
        managed = True
        db_table = 'app_boxcola'
        verbose_name = 'Cola en boxes'
        verbose_name_plural = 'Colas en boxes'


class EstadoBox(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=255, blank=True, null=True)
    codigo = models.CharField(max_length=10, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'estado_box'
        verbose_name = 'estado de un box'
        verbose_name_plural = 'estados de box'


class EstadoTicket(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    codigo = models.CharField(max_length=10, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_insercion = models.CharField(max_length=50, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = True
        db_table = 'estado_ticket'
        verbose_name = 'estado de un ticket'
        verbose_name_plural = 'estados de tickets'