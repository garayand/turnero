from django.contrib import admin
from .models import *


class TicketCallerAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["ticket","last_call"]
    list_filter = ("ticket","last_call")
    list_per_page = 10



class AreaAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","codigo_area","departamento"]
    list_filter = ("descripcion", "departamento")
    list_per_page = 10


class RolAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","codigo","activo"]
    list_filter = ('descripcion','activo')
    #list_editable=[]
    list_per_page = 10

class PermisoAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","codigo","activo"]
    list_filter = ('descripcion','activo')
    list_per_page = 10

class UsuarioAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","email"]
    list_filter = ('usuario','email')
    list_per_page = 10

class DepartamentoAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__"]
    list_filter = ('nombre',)
    list_per_page = 10

class BoxAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","estado_box","area","nombre"]
    list_filter = ("codigo","numero","estado_box","area")
    list_per_page = 10

class ColaAtencionAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","codigo"]
    list_filter = ('descripcion','codigo')
    list_per_page = 10

class TipoPersonaAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__"]
    list_filter = ('descripcion',)
    list_per_page = 10

class PersonaAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__", "tipo_persona","documento","sexo","fecha_nacimiento"]
    list_filter = ("nombres","apellidos","tipo_persona","documento","sexo","fecha_nacimiento")
    list_per_page = 10

class CategoriaClienteAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","codigo_categoria"]
    list_filter = ("descripcion","codigo_categoria")
    list_per_page = 10

class ClienteAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["get_persona",]
    list_filter = ("persona",)
    list_per_page = 10

    def get_persona(self, obj):
        return obj.persona.nombres + " " + obj.persona.apellidos

class FuncionarioAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["get_persona",]
    #list_filter = ("get_persona",)
    list_per_page = 10

    def get_persona(self, obj):
        return obj.persona.nombres + " " + obj.persona.apellidos

class ServicioAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__",]
    list_filter = ("nombre",)
    list_per_page = 10

class TicketAtencionAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__","estado_ticket"]
    list_filter = ("estado_ticket","numero_orden","prefijo",)
    list_per_page = 10

class NivelPrioridadAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__",]
    list_filter = ("descripcion",)
    list_per_page = 10

class EstadoBoxAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__",]
    list_filter = ("descripcion",)
    list_per_page = 10

class EstadoTicketAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display=["__str__",]
    list_filter = ("descripcion",)
    list_per_page = 10


class BoxColaAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_per_page = 10


# Register your models here.
admin.site.register(Area, AreaAdmin)
admin.site.register(Rol, RolAdmin)
admin.site.register(Permiso, PermisoAdmin)
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Box, BoxAdmin)
admin.site.register(ColaAtencion, ColaAtencionAdmin)
admin.site.register(TipoPersona, TipoPersonaAdmin)
admin.site.register(Persona, PersonaAdmin)
admin.site.register(CategoriaCliente, CategoriaClienteAdmin)
admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Funcionario, FuncionarioAdmin)
admin.site.register(Servicio, ServicioAdmin)
admin.site.register(TicketAtencion, TicketAtencionAdmin)
admin.site.register(NivelPrioridad, NivelPrioridadAdmin)
admin.site.register(EstadoBox, EstadoBoxAdmin)
admin.site.register(EstadoTicket, EstadoTicketAdmin)
admin.site.register(BoxCola, BoxColaAdmin)
admin.site.register(TicketCaller, TicketCallerAdmin)
