import json

from django.contrib.auth.decorators import login_required
from django.utils import timezone
import datetime
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from .forms import *
from .models import *


@login_required
def home(request):
    template = loader.get_template('home.html')
    return HttpResponse(template.render())


@csrf_exempt
def caller(request):
    if request.method == "GET":
        ultimos_llamados = []
        estado_espera = EstadoTicket.objects.get(codigo='ESP')
        ultimos_llamados = TicketCaller.objects.filter().order_by('-last_call')[:10]
        try:
            to_call = TicketCaller.objects.filter(ticket__estado_ticket=estado_espera)\
                .order_by('last_call').last()
            if to_call is not None:
                to_call = str(to_call.ticket)
            else:
                to_call = 'No hay tickets en espera'
            context = {
                'to_call': str(to_call),
                'ultimos_llamados': ultimos_llamados
            }
        except ObjectDoesNotExist:
            to_call = 'No hay tickets en espera'
            context = {
                'to_call': to_call,
                'ultimos_llamados': ultimos_llamados
            }
        return render(request, 'ticket_caller.html', context)


@csrf_exempt
@login_required
def vista_agente(request, pk, action):
    if request.method == "POST":
        called_action = request.POST.get("action", None)
        next_ticket = None
        message = ''
        last_called = request.POST.get("last_called", None)
        servicios = Servicio.objects.all().values('id', 'nombre')
        if called_action == 'llamar':
            boxes_colas = BoxCola.objects.filter(box_id=pk)
            estado_espera = EstadoTicket.objects.get(codigo='ESP')
            for item in boxes_colas:
                tickets_prioridad= TicketAtencion.objects.filter(prefijo='PRI', estado_ticket=estado_espera)
                if tickets_prioridad.count() > 0:
                    next_ticket = tickets_prioridad.order_by('fechahora_ingreso').first()
                else:
                    colas_box = ColaAtencion.objects.filter(pk=item.cola_id.id).order_by('nivel_prioridad__orden')
                    for cola in colas_box:
                        if cola.tickets_creados > 0:
                            servicios_cola = cola.servicios.all()
                            next_ticket = TicketAtencion.objects\
                                .filter(estado_ticket=estado_espera, servicio__in=servicios_cola)\
                                .order_by('fechahora_ingreso').first()

            if next_ticket is not None:
                selected_box = Box.objects.get(id=pk)
                selected_box.tickets_llamados.add(next_ticket)
                message = 'Llamando...'

                called_ticket = TicketCaller.objects.filter(ticket=next_ticket).last()
                if called_ticket:
                    called_ticket.last_call = datetime.datetime.now(tz=timezone.utc)
                    called_ticket.save()
                else:
                    called_ticket = TicketCaller(
                        ticket=next_ticket,
                        last_call=datetime.datetime.now(tz=timezone.utc)
                    )
                called_ticket.save()
            else:
                message = 'No hay tickets en espera'
                next_ticket = None

        if called_action == 'atender':
            selected_box = Box.objects.get(id=pk)
            next_ticket = selected_box.tickets_llamados.all().last()
            print(next_ticket)
            estado_atencion = EstadoTicket.objects.get(codigo='ATE')
            if next_ticket is not None and next_ticket.estado_ticket.codigo == 'ESP':
                message = 'Atendiendo...'
                try:
                    next_ticket.estado_ticket = estado_atencion
                    next_ticket.fechahora_atencion = datetime.datetime.now(tz=timezone.utc)
                    next_ticket.usuario_modificacion = request.user.username
                    next_ticket.fecha_modificacion = datetime.datetime.now(tz=timezone.utc)
                    #TODO: modificar para actualizar el funcionario
                    next_ticket.funcionario = Funcionario.objects.get(id=1)
                    next_ticket.save()
                    selected_box.tickets_llamados.filter(pk=next_ticket.id)
                except Exception as e:
                    print(e)
            else:
                message = 'No hay tickets pendientes de atencion'
                next_ticket = None
                print('No hay ticket para atender')

        elif called_action == 'derivar':
            selected_box = Box.objects.get(id=pk)

            last_called = selected_box.tickets_llamados.all().last()
            next_ticket = TicketAtencion.objects.get(pk=last_called.id)
            selected_servicio = request.POST.get("servicio", None)
            try:
                servicio = Servicio.objects.get(pk=selected_servicio)
            except ObjectDoesNotExist:
                servicio = None
                print("Either the servicio doesn't exist.")

            colas = Servicio.objects.get(id=servicio.id).colaatencion_set.all()
            min = colas[0].tickets_creados
            cola_menor = colas[0]
            for cola in colas:
                if cola.descripcion != 'PRIORIDAD' and cola.tickets_creados < min:
                    min = cola.tickets_creados
                    cola_menor = cola
            if next_ticket is not None and next_ticket.estado_ticket.codigo == 'ATE':
                message = 'Se ha derivado al ticket '
                estado_espera = EstadoTicket.objects.get(codigo='ESP')
                next_ticket.estado_ticket = estado_espera
                next_ticket.servicio = servicio
                next_ticket.usuario_modificacion=request.user.username
                next_ticket.fecha_modificacion=datetime.datetime.now(tz=timezone.utc)
                next_ticket.save()
                cola_menor.tickets_creados += 1
                cola_menor.save()
            else:
                message = 'No hay tickets para derivar'
                next_ticket = None
                print('No hay tickets para derivar')

        elif called_action == 'terminar':
            selected_box = Box.objects.get(id=pk)
            estado_atencion = EstadoTicket.objects.get(codigo='FIN')
            last_called = selected_box.tickets_llamados.all().last()
            next_ticket = TicketAtencion.objects.get(pk=last_called.id)
            if next_ticket is not None and next_ticket.estado_ticket.codigo != 'FIN':
                message = 'Finalizando atencion...'
                next_ticket.estado_ticket = estado_atencion
                next_ticket.usuario_modificacion = request.user.username
                next_ticket.fecha_modificacion = datetime.datetime.now(tz=timezone.utc)
                next_ticket.save()
            else:
                message = 'No hay tickets en atencion'
                next_ticket = None
                print('No hay ticket en atencion')

        context = {
            'ticket_str': str(next_ticket),
            'servicios': list(servicios),
            'box': pk,
            'estado': message,
        }
        return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        servicios = Servicio.objects.all()
        context = {
            'servicios': servicios,
            'box': pk
        }
        return render(request, 'vista_agente.html', context)


def call_ticket_agente(request, box_id, action):
    colas_box = BoxCola.objects.filter(box_id=box_id)
    template = loader.get_template('vista_agente.html')
    return HttpResponse(template.render())


@login_required
def select_box(request):
    if request.method == 'GET':
        estado=EstadoBox.objects.get(codigo='LIB')
        boxes_libres = Box.objects.filter(estado_box=estado)
        return render(request, 'select_box.html', {'boxes': boxes_libres})


@login_required
def servicio_list(request, pk, priority):
    cliente = Cliente.objects.get(pk=pk)
    list_servicios = Servicio.objects.all().values()

    if request.method == 'POST':
        cliente_id = request.POST['cliente']
        servicio_id = request.POST['servicio']

        try:
            cliente = Cliente.objects.get(pk=cliente_id)
        except ObjectDoesNotExist:
            cliente = None
            print("Either the client doesn't exist.")

        try:
            servicio = Servicio.objects.get(pk=servicio_id)
        except ObjectDoesNotExist:
            servicio = None
            print("Either the servicio doesn't exist.")
        ticket = None
        try:
            if priority == 'True':
                cola_menor= ColaAtencion.objects.get(descripcion='PRIORIDAD')
            else:
                colas = Servicio.objects.get(id=servicio.id).colaatencion_set.all()
                min = colas[0].tickets_creados
                cola_menor = colas[0]

                for cola in colas:
                    if cola.descripcion != 'PRIORIDAD' and cola.tickets_creados < min:
                        min = cola.tickets_creados
                        cola_menor = cola
            try:
                #CREAR TICKET DE ATENCION
                new_ticket = TicketAtencion(
                    estado_ticket=EstadoTicket.objects.get(codigo='ESP'),
                    servicio=servicio,
                    funcionario=None, #TODO: CAMBIAR POR EL FUNCIONARIO QUE ATIENDE
                    cliente=cliente,
                    prefijo=cola_menor.codigo,
                    numero_orden=cola_menor.tickets_creados + 1,
                    fechahora_ingreso=datetime.datetime.now(tz=timezone.utc),
                    fechahora_atencion=None,
                    usuario_modificacion=request.user,
                    fecha_modificacion=datetime.datetime.now(tz=timezone.utc),
                    usuario_insercion=request.user,
                    fecha_insercion=datetime.datetime.now(tz=timezone.utc),
                )
                new_ticket.save()
                ticket = TicketAtencion.objects.get(id=new_ticket.id)
                cola_menor.tickets_creados += 1
                cola_menor.save()
                return render(request, 'show_ticket.html', {'ticket': ticket})
            except Exception as e:
                print(e)
        except Exception as exc:
            colas = None
            print("Este servicio no esta asociado a ninguna cola", exc)
    else:
        template = loader.get_template('select_servicio.html')
        context = {
            'priority': priority,
            'cliente': cliente,
            'list_servicios': list_servicios,
        }
        return HttpResponse(template.render(context, request))

@login_required
def input_cedula(request):
    if request.method == 'POST':
        form = InputCedulaForm(request.POST)
        if form.is_valid():
            nro_documento = form.cleaned_data['nro_documento']
            priority = form.cleaned_data['es_prioridad']
            try:
                persona = Persona.objects.get(documento=nro_documento)
            except ObjectDoesNotExist:
                print("Either the blog or entry doesn't exist.")
                persona = Persona.objects.get(documento="000000")
            cliente = Cliente.objects.get(persona=persona)
            return redirect('servicios', pk=cliente.id, priority=priority)
    else:
        form = InputCedulaForm()
        return render(request, 'input_cedula.html', {'form': form})
