# Generated by Django 4.1.7 on 2023-02-20 14:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='box',
            name='colas',
        ),
        migrations.RemoveField(
            model_name='colaatencion',
            name='nivel_prioridad',
        ),
        migrations.CreateModel(
            name='BoxCola',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('box_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.box')),
                ('cola_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.colaatencion')),
                ('nivel_prioridad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.nivelprioridad')),
            ],
        ),
    ]
