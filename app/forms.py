from django import forms


class InputCedulaForm(forms.Form):
    nro_documento = forms.CharField(label='Ingrese el numero de documento del cliente:', max_length=10)
    es_prioridad = forms.BooleanField(label='Indique si el cliente tiene prioridad', required=False)


class SelectServicioForm(forms.Form):
    nro_documento = forms.CharField(label='Ingrese el numero de documento del cliente:', max_length=10)
    es_prioridad = forms.BooleanField(label='Indique si el cliente tiene prioridad', required=False)



