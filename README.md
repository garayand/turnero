# turnero



## Getting started

Realizar la migracion desde la base de datos del proyecto para crear los modelos.
La base de datos debe tener por nombre "turnero" y debe estar en postgresql.

Para empezar a usar el sistema se debe ingresar a la URL:
localhost:8000/home.

Debe tener creado un superusuario en django con permisos de administrador.

Desde la interfaz de administracion debe crear una entidad:
Persona, Cliente o Funcionario.

Luego debe crear los Servicios, las Colas y asociarlos en la entidad Boxes en Colas.

Debe crear una cola para clientes con prioridad.
Debe crear una persona con numero de documento "000000" para invitados, crear un cliente y asociarlo a esta persona.

Levantar el proyecto en el servidor de desarrollo de django.
